<?php
/**
 * Created by PhpStorm.
 * User: gxfang
 * Date: 15-12-9
 * Time: 下午4:40
 */
//-- 用户端:用户登录-------------------------
try {
    include "../includes/public_header.php";

    $mobile   = $_REQUEST["mobile"] == null ? "" : $_REQUEST["mobile"];
    $password = $_REQUEST["password"] == null ? "" : $_REQUEST["password"];

    $arrayx = array();
    if ( $mobile == "" || $password == "" ) {

        $arrayx["success"] = 0;
        $arrayx["errinfo"] = "参数不全!";
        $arrayx["errcode"] = "-1";
        $callback          = isset( $_REQUEST['callback'] ) ? trim( $_REQUEST['callback'] ) : '';

        echo $callback . '(' . json_encode( $arrayx ) . ')';
        die;
    } else {

        $x_sql = "select * from user where user_password='" . md5($password) . "' and user_mobile='" . $mobile . "' ";
        log_result_mini4( $x_sql );
        $res         = $db->query( $x_sql );
        $ifok        = 0;
        $verifystate = 0;
        $uid         = "";
        $uname       = "";
        $upic        = "";
        $upic_small  = "";
        $upic_middle = "";
        $umobile     = "";

        $usex = "";
        if ( $db->next_record() ) {
            $ifok        = 1;
            $uid         = $db->f( "id" );
            $uname       = $db->f( "user_name" );
            $upic        = $db->f( "pic" );
            $upic_small  = $db->f( "pic_small" );
            $upic_middle = $db->f( "pic_middle" );

            $umobile = $db->f( "user_mobile" );
//            $usex    = $db->f( "sex" );
        }


        if ( $ifok >= 1 )
        {
            $arrayx            = array();
            $arrayx["success"] = 1;
            $arrayx["code"]    = "200";
            $arrayx["uid"]     = $uid;
            $arrayx["uname"]   = $uname;
            $arrayx["upic"]    = $upic;

            $arrayx["upic_small"]  = $upic_small;
            $arrayx["upic_middle"] = $upic_middle;
            $arrayx["umobile"]     = $umobile;
//            $arrayx["usex"]        = $usex;


            $callback = isset( $_REQUEST['callback'] ) ? trim( $_REQUEST['callback'] ) : '';
            echo $callback . '(' . json_encode( $arrayx ) . ')';

            die;

        } else {
            $arrayx            = array();
            $arrayx["success"] = 0;
            $arrayx["errinfo"] = "手机号或密码不正确,请重新填写!";
            $arrayx["errcode"] = "-1";

            $callback = isset( $_REQUEST['callback'] ) ? trim( $_REQUEST['callback'] ) : '';
            echo $callback . '(' . json_encode( $arrayx ) . ')';
        }
    }


} catch ( Exception $e ) {
    $arrayx            = array();
    $arrayx["success"] = 0;
    $arrayx["errinfo"] = $e->getMessage();
    $arrayx["errcode"] = "-99";


    $callback = isset( $_REQUEST['callback'] ) ? trim( $_REQUEST['callback'] ) : '';
    echo $callback . '(' . json_encode( $arrayx ) . ')';

}


?>