<?php
/**
 * Created by PhpStorm.
 * User: gxfang
 * Date: 15-12-9
 * Time: 下午5:53
 */

//--用户端： 用户注册

try {
    include "../includes/public_header.php";

    $mobile   = $_REQUEST["mobile"] == null ? "" : $_REQUEST["mobile"];
    $captcha  = $_REQUEST["captcha"] == null ? "" : $_REQUEST["captcha"];
    $password = $_REQUEST["password"] == null ? "" : $_REQUEST["password"];

    $arrayx = array();
    if ( $mobile == "" || $captcha == "" || $password == "" ) {
        $arrayx["success"] = 0;
        $arrayx["errcode"] = "-1";
        $arrayx["errinfo"] = "信息不全";

        $callback = isset( $_REQUEST['callback'] ) ? trim( $_REQUEST['callback'] ) : '';
        echo $callback . '(' . json_encode( $arrayx ) . ')';
        die;
    }

    $x_sql = "select * from user where user_mobile='" . $mobile . "'";
    log_result_mini4( $x_sql );
    $res      = $db->query( $x_sql );
    $ifexist2 = 0;
    $uid      = "";
    if ( $db->next_record() ) {
        $ifexist2 = 1;
        $uid      = $db->f( "id" );
    }
    if ( $ifexist2 >= 1 ) {

        $arrayx["success"] = 0;
        $arrayx["errcode"] = "-2";
        $arrayx["errinfo"] = "该手机号已被注册，请换其它号码！";

        $callback = isset( $_REQUEST['callback'] ) ? trim( $_REQUEST['callback'] ) : '';
        echo $callback . '(' . json_encode( $arrayx ) . ')';
        die;
    }

//-----------------------------------------验证码   Start   type =4 ----------------------------------------------------------------------------------------------------

    $x_sql = "select 1,( UNIX_TIMESTAMP(now())-UNIX_TIMESTAMP(datetime) ) as diff from user_captcha  where  type=4  and  captcha='" . $captcha . "'";
    log_result_mini4( $x_sql );
    $res     = $db->query( $x_sql );
    $ifexist = 0;
    $diff    = 0;
    if ( $db->next_record() ) {

        $ifexist = 1;
        $diff    = $db->f( "diff" );
    }

    if ( $ifexist < 1 ) {

        $arrayx["success"] = 0;
        $arrayx["errcode"] = "-3";
        $arrayx["errinfo"] = "验证码不存在!";
        $callback          = isset( $_REQUEST['callback'] ) ? trim( $_REQUEST['callback'] ) : '';
        echo $callback . '(' . json_encode( $arrayx ) . ')';
        die;
    } else {
        if ( $diff > 120 ) {

            $arrayx["success"] = 0;
            $arrayx["code"]    = 0;
            $arrayx["errcode"] = "-4";
            $arrayx["errinfo"] = "验证码已过期失效!";
            $callback          = isset( $_REQUEST['callback'] ) ? trim( $_REQUEST['callback'] ) : '';
            echo $callback . '(' . json_encode( $arrayx ) . ')';
            die;
        } else {


            $x_sql = "select 1,mobile from user_captcha  where  type=4 and  captcha='" . $captcha . "'";
            log_result_mini4( $x_sql );
            $res     = $db->query( $x_sql );
            $ifexist = 0;
            $mobile  = "";
            if ( $db->next_record() ) {
                $mobile  = $db->f( "mobile" );
                $ifexist = 1;
            }

            if ( $ifexist < 1 ) {
                $arrayx["success"] = 0;
                $arrayx["code"]    = 0;
                $arrayx["errcode"] = "-4";
                $arrayx["errinfo"] = "验证码与您的注册手机不匹配!";

                $callback = isset( $_REQUEST['callback'] ) ? trim( $_REQUEST['callback'] ) : '';
                echo $callback . '(' . json_encode( $arrayx ) . ')';
                die;

            } else {
            }
        }
    }
//-----------------------------------------验证码   End----------------------------------------------------------------------------------------------------

    $x_sql = "insert into user (user_mobile,user_password) values ('".$mobile."','".md5($password)."')";

    log_result_mini4( $x_sql );
    $res       = $db->query( $x_sql );
    $insert_id = $db->insert_id( $res );


    $arrayx            = array();
    $arrayx["success"] = 1;
    $arrayx["errcode"] = 0;

    $arrayx["uid"] = $insert_id;

    $callback = isset( $_REQUEST['callback'] ) ? trim( $_REQUEST['callback'] ) : '';
    echo $callback . '(' . json_encode( $arrayx ) . ')';


} catch ( Exception $e ) {
    $arrayx            = array();
    $arrayx["success"] = 0;
    $arrayx["errinfo"] = $e->getMessage();
    $arrayx["errcode"] = "-99";


    $callback = isset( $_REQUEST['callback'] ) ? trim( $_REQUEST['callback'] ) : '';
    echo $callback . '(' . json_encode( $arrayx ) . ')';

}

?>