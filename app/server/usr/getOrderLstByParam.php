<?php
//--取得订单———————————
try
{
	include "../includes/public_header.php";
    //session_start(); 
    //$sessionid=session_id();

	$arrayx= array();
   
	//根据用户ID取得订单列表信息
	$uid=$_REQUEST["uid"]==null?"":$_REQUEST["uid"];
	$maxid=$_REQUEST["maxid"]==null?"":$_REQUEST["maxid"];
	$order_status=$_REQUEST["order_status"]==null?"0":$_REQUEST["order_status"];
	if($uid!="")
	{
		$res=null;
		$res2=null;
		//进行中
		if ($order_status=="0")
		{	
			$x_sql="select t.id as id, t.order_no as orderNo, t.contact as contactName, t.tel as contactMob, t.weixin as contactWeixin, t.service_type as serviceType, t.person_count as pNo, t.adult_no as adultNo, t.child_no as childNo, t.baby_no as babyNo, t.emergency_contact as contact, t.emergency_mobile as mobile, t.flight_time as serviceTime, t.flight_no as flightNo, t.airport_id as airportID, a.airport_name as airportName, t.start_time as startdate, t.end_time as enddate, t.day as day, t.start_city_id as cityID, t.start_city_name as cityName, t.travel_city_id_lst as cityPassed, t.travel_city_name_lst as cityPassedNames, t.start_address as startAddress, t.end_address as endAddress, t.cartype_id as carTypeID, t.has_line_product as recmdline, t.line_id as lineID, t.line_title as linetitle, l.days as lineDays, t.itinerary_pic_lst as images, t.memo as memo, t.amount_price as amount, t.line_price as linePrice, t.car_price as autoPrice, t.lodging_price as livePrice, t.order_status as orderStatus, t.order_origin as orderOrigin, t.operator_id as operatorID, t.createtime as createTime, t.updatetime as updateTime from tour_order t left join pref_airport a on t.airport_id = a.airport_code left join tour_line l on t.line_id = l.id where t.order_status < 6 and t.order_status <> '-1' and t.id in (select oid from user_order where uid = '".$uid."') and t.id > '".$maxid."' order by startdate";
			$res=$db2->queryArray($x_sql);
			
			$x_sql="select MAX(t.id) as maxid from tour_order t where t.order_status < 6 and t.id in (select oid from user_order where uid = '".$uid."')";
			$res2=$db2->query($x_sql);
		}
		//已完成
		else if ($order_status=="1")
		{
			$x_sql="select t.id as id, t.order_no as orderNo, t.contact as contactName, t.tel as contactMob, t.weixin as contactWeixin, t.service_type as serviceType, t.person_count as pNo, t.adult_no as adultNo, t.child_no as childNo, t.baby_no as babyNo, t.emergency_contact as contact, t.emergency_mobile as mobile, t.flight_time as serviceTime, t.flight_no as flightNo, t.airport_id as airportID, a.airport_name as airportName, t.start_time as startdate, t.end_time as enddate, t.day as day, t.start_city_id as cityID, t.start_city_name as cityName, t.travel_city_id_lst as cityPassed, t.travel_city_name_lst as cityPassedNames, t.start_address as startAddress, t.end_address as endAddress, t.cartype_id as carTypeID, t.has_line_product as recmdline, t.line_id as lineID, t.line_title as linetitle, l.days as lineDays, t.itinerary_pic_lst as images, t.memo as memo, t.amount_price as amount, t.line_price as linePrice, t.car_price as autoPrice, t.lodging_price as livePrice, t.order_status as orderStatus, t.order_origin as orderOrigin, t.operator_id as operatorID, t.createtime as createTime, t.updatetime as updateTime from tour_order t left join pref_airport a on t.airport_id = a.airport_code left join tour_line l on t.line_id = l.id where (t.order_status = '6' or t.order_status = '7') and t.id in (select oid from user_order where uid = '".$uid."') and t.id > '".$maxid."' order by startdate";
			$res=$db2->queryArray($x_sql);

			$x_sql="select MAX(t.id) as maxid from tour_order t where t.order_status = '6' and t.order_status = '7' and t.id in (select oid from user_order where uid = '".$uid."')";
			$res2=$db2->query($x_sql);
		}
		//已取消
		else if ($order_status=="2")
		{
			$x_sql="select t.id as id, t.order_no as orderNo, t.contact as contactName, t.tel as contactMob, t.weixin as contactWeixin, t.service_type as serviceType, t.person_count as pNo, t.adult_no as adultNo, t.child_no as childNo, t.baby_no as babyNo, t.emergency_contact as contact, t.emergency_mobile as mobile, t.flight_time as serviceTime, t.flight_no as flightNo, t.airport_id as airportID, a.airport_name as airportName, t.start_time as startdate, t.end_time as enddate, t.day as day, t.start_city_id as cityID, t.start_city_name as cityName, t.travel_city_id_lst as cityPassed, t.travel_city_name_lst as cityPassedNames, t.start_address as startAddress, t.end_address as endAddress, t.cartype_id as carTypeID, t.has_line_product as recmdline, t.line_id as lineID, t.line_title as linetitle, l.days as lineDays, t.itinerary_pic_lst as images, t.memo as memo, t.amount_price as amount, t.line_price as linePrice, t.car_price as autoPrice, t.lodging_price as livePrice, t.order_status as orderStatus, t.order_origin as orderOrigin, t.operator_id as operatorID, t.createtime as createTime, t.updatetime as updateTime from tour_order t left join pref_airport a on t.airport_id = a.airport_code left join tour_line l on t.line_id = l.id where t.order_status = '-1' and t.id in (select oid from user_order where uid = '".$uid."') and t.id > '".$maxid."' order by startdate";
			$res=$db2->queryArray($x_sql);
			
			$x_sql="select MAX(t.id) as maxid from tour_order t where  t.order_status = '-1' and t.id in (select oid from user_order where uid = '".$uid."')";
			$res2=$db2->query($x_sql);
		}
		
		$arrayx["success"]=1;
		$arrayx["code"]="200";
		$arrayx["errcode"]="0";
		$arrayx["errinfo"]="";
		$arrayx["record"]=$res;
		if ($res2)
		{
			$arrayx[$order_status]=$res2["maxid"];
		}
		else
		{
			$arrayx[$order_status]=0;
		}	
		
	}
	else
	{
		$arrayx["success"]=0;
		$arrayx["code"]="200";
		$arrayx["errcode"]="1";
		$arrayx["errinfo"]="没有取得用户ID";
	}
         
	$callback = isset($_REQUEST['callback']) ? trim($_REQUEST['callback']) : '';
	echo $callback . '(' . json_encode($arrayx) .')'; 
     	
	die;
}
catch(Exception $e) 
{
	
	$return= "{\"success\":0,\"errinfo\":".$e->getMessage()."}";
	   
//print $e->getMessage();   
//exit();   
}   
echo $return;

?>