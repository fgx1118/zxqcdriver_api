<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: text/plain');
include "../../includes/public_header.php";

// 对签名字符串转义
function createLinkstring($para) {
	$arg = "";
	while (list($key, $val) = each($para)) {
		$arg .= $key . '="' . $val . '"&';
	}
	//去掉最后一个&字符
	$arg = substr($arg, 0, count($arg) - 2);
	//如果存在转义字符，那么去掉转义
	if (get_magic_quotes_gpc()) {$arg = stripslashes($arg);
	}
	return $arg;
}

// 签名生成订单信息
function rsaSign($data) {
	//  %私钥%";   // 生成密钥时获取，直接使用pem文件的字符串

	$priKey = "-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQDAABXC/BgbW8gFZ8jTJHGuJiTy3TY5mkEWM3KDH2PgTyCNrj63
WNsOwcNXp/LxoJlcG2DaLv9mW4wbjsZApHGK2fuQaYoLU7lKgUexE99PuVIPhG0h
jjYh5ZnwkaLppGh/Ct2PKyem5bw14wXWK8ux9HcF8hRSVCNe0NK1nkZF3QIDAQAB
AoGAEdIwMuf5SNjNCQtoDYkNcC+kMFKuDMyvi+L8lwUb3R8bE2t/8L6znnwHqKQz
HZBdi7oS1E2WcSbAK3KmHivlSeff9+mrs09TTMKLDyUrBuASRxDMlENWyx4cwcBq
2RY2JoS2JwX/fDPV/eSZHWNI0arC7gYo/VzUzbKj6+3Z3IECQQDmVtYbxsYLia20
d8iSe8sW12BoDK9kxHH1UdrEW2z4e6f7HZ8BK22soCW2PTSLHGVSoRMTiikGqcXy
BuwZGtOhAkEA1WPXpt83Lr3FNTB2krhmlQw/oDNJYq03tQgqHxD3YzFy/YqAgeui
QKJzTRqyXEnregpv5ge4L+HxxrKYwPsIvQJAdDVLQChAsoF7iZkFMCIUjCmNEfGU
a5pQhBwP6xDVIWgBj8eZ+NUYOAdz/0VRxP1GgTGSlkEWAa6Vl022v9OxAQJAEfx8
bOBbsmDkgns3Q16vptiFdGdeRCb9jxDFHCE9+OpuGtjVxB2sa25F8bXSWt0QMBU5
IVZ9O+v7iRf8sCd4sQJBALYXkJ15p0cnfHz7U7WUuufgNX+ZpzDhFwoDdDNR39HZ
pvYzxq206WS1/ShAVswRKON8IhZVRvE5nl8bv0U7H2o=
-----END RSA PRIVATE KEY-----";
	$res = openssl_get_privatekey($priKey);
	openssl_sign($data, $sign, $res);
	openssl_free_key($res);
	$sign = base64_encode($sign);
	$sign = urlencode($sign);
	return $sign;
}

$arrayx = array();

$oid = ($_REQUEST["oid"] == null ? "" : $_REQUEST["oid"]);

try {

	if ($oid == "") {
		$arrayx["success"] = 0;
		$arrayx["errcode"] = "-1";
		$arrayx["errinfo"] = "订单编号不能为空";

	} else {

		$x_sql = "select amount_price,order_no from tour_order where id='" . $oid . "'";
		$db -> query($x_sql);
		$amount = '';
		$order_no = '';
		if ($db -> next_record()) {
			$amount = $db -> f("amount_price");
			$order_no = $db -> f("order_no");
		}
		/*
		 // 获取支付金额
		 if($_SERVER['REQUEST_METHOD']=='POST'){
		 $amount=$_POST['total'];
		 }else{
		 $amount=$_GET['total'];
		 }
		 */
		$total = floatval($amount);
		//$oid=$_REQUEST["oid"];

		if (!$total) {
			$total = 10;
		}

		// 支付宝合作身份者ID，以2088开头的16位纯数字
		$partner = "2088911908052351";
		// 支付宝开通快捷支付功能后可获取
		// 支付宝账号
		$seller_id = "alex.zhang@etjourney.com";
		// 商品网址
		$base_path = urlencode('http://www.etjourney.com/project/baoche/app/server/usr/payment/');
		// 异步通知地址
		$notify_url = urlencode('http://www.etjourney.com/project/baoche/app/server/usr/alipay/notify.php');
		// 订单标题
		$subject = '坐享其成包车订单';
		// 订单详情
		$body = '坐享其成包车订单';
		// 订单号，示例代码使用时间值作为唯一的订单ID号

		$out_trade_no = $order_no;
		//date('YmdHis', time());  // $oid;//
		$parameter = array('service' => 'mobile.securitypay.pay', // 必填，接口名称，固定值
		'partner' => $partner, // 必填，合作商户号
		'_input_charset' => 'UTF-8', // 必填，参数编码字符集
		'out_trade_no' => $out_trade_no, // 必填，商户网站唯一订单号
		'subject' => $subject, // 必填，商品名称
		'payment_type' => '1', // 必填，支付类型
		'seller_id' => $seller_id, // 必填，卖家支付宝账号
		'total_fee' => $total, // 必填，总金额，取值范围为[0.01,100000000.00]
		'body' => $body, // 必填，商品详情
		'it_b_pay' => '1d', // 可选，未付款交易的超时时间
		'notify_url' => $notify_url, // 可选，服务器异步通知页面路径
		'show_url' => $base_path // 可选，商品展示网站
		);
		//生成需要签名的订单
		$orderInfo = createLinkstring($parameter);
		//签名
		$sign = rsaSign($orderInfo);
		//生成订单

		$info = $orderInfo . '&sign="' . $sign . '"&sign_type="RSA"';

		$arrayx["success"] = 1;
		$arrayx["errcode"] = "200";
		$arrayx["orderinfo"] = $info;

	}
	echo $info;

	/*   $callback = isset($_REQUEST['callback']) ? trim($_REQUEST['callback']) : '';
	 echo $callback . '(' . json_encode($arrayx) .')';
	 */

} catch(Exception $e) {

	$return = "{\"success\":0,\"errinfo\":" . $e -> getMessage() . "}";
	echo $return;

	//print $e->getMessage();
	//exit();
}
?>