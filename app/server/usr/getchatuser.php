<?php
//--客户端:获取可以聊天的用户----------------------
try {
	$uid = ($_REQUEST["uid"] == null ? "" : $_REQUEST["uid"]);
	$arrayx = array();
	if ($uid == "") {
		$arrayx["success"] = 0;
		$arrayx["errinfo"] = "参数不全!";
		$arrayx["errcode"] = "-1";
		$callback = isset($_REQUEST['callback']) ? trim($_REQUEST['callback']) : '';

		echo $callback . '(' . json_encode($arrayx) . ')';
		die ;
	} else {
		include "../includes/public_header.php";
		$start_dt = date('Y-m-d H:i:s',time()+259200);
		$end_dt_1 = date('Y-m-d H:i:s');
		$end_dt_2 = date('Y-m-d H:i:s',time()-86400); //接送机无结束时间，按开始时间一天内
		//$x_sql = "select a.id,a.order_no,a.start_time,c.name dname,c.id did,c.pic_small from tour_order a left join driver_qd b on b.oid = a.id left join driver c on c.id = b.did where a.operator_id =" . $uid . " and a.order_origin = 0 and a.order_status > 1 and b.order_verify_flg = 1 group by a.id order by a.id desc";
		$x_sql = "select a.id,a.order_no,a.start_time,c.name dname,c.id did,c.pic_small from tour_order a left join driver_qd b on b.oid = a.id left join driver c on c.id = b.did "
						."where a.id in (select oid from user_order where uid = '".$uid."') and a.order_status > 1 and b.order_verify_flg = 1 "
						."and a.start_time <= '".$start_dt."' and ((a.end_time is null and a.start_time >= '".$end_dt_2."') or (a.end_time is not null and a.end_time >= '".$end_dt_1."')) "
						." order by a.id desc";
		setLog('sql"'.$x_sql);
		$res = $db -> queryArray($x_sql);

		$arrayx = array();
		$arrayx["success"] = 1;
		$arrayx["code"] = "200";
		$arrayx["errcode"] = "0";
		$arrayx["errinfo"] = "";
		$arrayx["record"] = $res;
		$arrayx["count"] = count($res);

		$callback = isset($_REQUEST['callback']) ? trim($_REQUEST['callback']) : '';
		echo $callback . '(' . json_encode($arrayx) . ')';

	}

} catch(Exception $e) {
	$arrayx = array();
	$arrayx["success"] = 0;
	$arrayx["errinfo"] = $e -> getMessage();
	$arrayx["errcode"] = "-99";

	$callback = isset($_REQUEST['callback']) ? trim($_REQUEST['callback']) : '';
	echo $callback . '(' . json_encode($arrayx) . ')';
}
?>