<?php
//--获取价格———————————
try
{
	include "../includes/public_header.php";
	$arrayx= array();
	
	//根据cited求出region_code
	$cityid=$_REQUEST["cityid"]==null?"":$_REQUEST["cityid"];
	$service_type =$_REQUEST["service_type"]==null?"":$_REQUEST["service_type"];
	$car_id =$_REQUEST["car_id"]==null?"":$_REQUEST["car_id"];
	$days =$_REQUEST["days"]==null?"":$_REQUEST["days"];
	if($cityid=='' || $service_type=='' || $car_id=='' || $days=='')
	{
		$arrayx["success"] = 0;
		$arrayx["errcode"] = "-1";
		$arrayx["errinfo"] = "参数错误";
		$arrayx["code"] = "-1";
	}
	else
	{
		$x_sql="select region_id from pref_city where city_code = '".$cityid."'";
		$res=$db2->queryArray($x_sql);
		if(count($res) > 0)
		{
			$region_id = $res[0]['region_id'];
			if ($service_type=="2")
			{
				//城际包车的情况下
				//根据region_code求出食宿价格
				$x_sql="select price from pref_accommodation_price where region_id = '".$region_id."'";
				$res_acc_price=$db->queryArray($x_sql);
				if(count($res_acc_price) > 0)
					$livePrice = $res_acc_price[0]['price'];
			}
			else
				$livePrice = 0;
			
			//根据服务类型，区域code，车辆型号求出包车价格
			$x_sql="select price from pref_autoservice_price where service_type = '".$service_type."' and region_id = '".$region_id."' and cartype_id = '".$car_id."'";
			$res_auto_price=$db->queryArray($x_sql);
			if(count($res_auto_price) > 0)
				$autoPrice = $res_auto_price[0]['price'];
			
			if(!isset($livePrice))
			{
				$arrayx["success"] = 0;
				$arrayx["errcode"] = "-3";
				$arrayx["errinfo"] = "未找到该城市食宿价格";
				$arrayx["code"] = "-3";
			}
			else if(!isset($autoPrice))
			{
				$arrayx["success"] = 0;
				$arrayx["errcode"] = "-4";
				$arrayx["errinfo"] = "未找到该城市对应车型包车价格";
				$arrayx["code"] = "-4";
			}
			else
			{
				$info=array();
				$info['live'] = $livePrice * ($days - 1);
				$info['auto'] = $autoPrice * $days;
				$arrayx["success"] = 1;
				$arrayx["errcode"] = "0";
				$arrayx["errinfo"] = "";
				$arrayx["record"] = $info;
				$arrayx["code"] = "200";
			}
		}
		else
		{
			$arrayx["success"] = 0;
			$arrayx["errcode"] = "-2";
			$arrayx["errinfo"] = "该城市不存在";
			$arrayx["code"] = "-2";
		}
	}
	//echo '<pre>';print_r($arrayx);exit;
	$callback = isset($_REQUEST['callback']) ? trim($_REQUEST['callback']) : '';
	echo $callback . '(' . json_encode($arrayx) . ')';
	die;
}
catch(Exception $e) 
{
	$return= "{\"success\":0,\"errinfo\":".$e->getMessage()."}";
}   
echo $return;
?>